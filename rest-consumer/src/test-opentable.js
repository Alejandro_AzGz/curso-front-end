import { LitElement, html, css } from 'lit-element';

class TestOpentable extends LitElement {
    static get styles() {
        return css`
            :host {
                display: block;
            }
            td {
                text-align: center;
            }
            button { float:left }
        `
    }

    static get properties() {
        return { 
            resultados: {type:Object},
            campos: {type:Array},
            filas: {type:Array},
            sig: {type:Array},
            prev: {type:Boolean},
            url: {type:String}
        };
    }

    constructor() {
        super();
        this.campos = [];
        this.resultados = { results: []};
        this.filas = [];
        this.url = "https://swapi.dev/api/planets"
        this.cargarDatos(this.url);
    }

    render() {
        return html`
            ${this.prev?
                html`<div><button @click=${this.anterior}>Anterior</button></div>`: html``
            }
            ${this.prev?
                html`<div><button @click=${this.siguiente}>Siguiente</button></div>`: html``
            }
            <table width="100%">
                <tr>
                    ${this.campos.map(c => html`<th>${c}</th>`)}
                </tr>
                ${this.filas.map(i => html`<tr>
                    ${this.resultados.results[i].valores.map(v => html`
                        <td>${v}</td>
                    `)}
                </tr>`)}
            </table>
        `
    }

    cargarDatos(url) {
        fetch(url)
        .then((response) => {
            console.log(response);
            if(!response.ok) { throw response; }
            return response.json();
        })
        .then((data) => {
            this.resultados = data;
            this.mostrarResultados();
        })
        .catch(error => {
            alert("Problemas con el fetch", error);
        });;
    }

    mostrarResultados() {
        this.campos = this.getObjProps(this.resultados.results[0]);
        this.filas = [];
        for (let i = 0; i < this.resultados.results.length; i++) {
            this.resultados.results[i].valores = this.getValores(this.resultados.results[i]);
            this.filas.push(i);
        }

        this.sig = {}
    }

    getValores(fila) {
        var valores = [];
        for(var i = 0; i < this.resultados.results.length; i++) {
            valores[i] = fila[this.campos[i]];
        }
        return valores;
    }

    getObjProps(obj) {
        var props = [];
        for (const prop in obj) {
            if (!this.isArray(this.resultados.results[0][prop])) {
                if((prop !== "created") && (prop !== "edited") && (prop !== "url")) {
                    props.push(prop);
                }
            }
        }
        return props;
    }

    isArray(prop) {
        return Object.prototype.toString.call(prop) === "[object Array]";
    }

    anterior() {
        this.cargarDatos(this.resultados.previous);
    } 

    siguiente() {
        this.cargarDatos(this.resultados.next);
    } 
}
customElements.define('test-opentable', TestOpentable);