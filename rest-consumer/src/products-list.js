import { LitElement, html, css } from 'lit-element';

class ProductsList  extends LitElement {

  static get styles() {
    return css`
      :host {
        display: block;
      }
    `;
  }

  static get properties() {
    return {
        productos: {type: Array}
    };
  }

  constructor() {
    super();
    this.productos=[];
    this.productos.push({"nombre": "Movil XL", "descripcion": "Un telefono aca bien chido con pila chida."});
    this.productos.push({"nombre": "Movil mini", "descripcion": "Un telefono chido pero no tan chido como el chido."});
    this.productos.push({"nombre": "Movil Standard", "descripcion": "Un telefono bien feo, la neta no lo compres."});
  }

  render() {
    return html`
      <div class="contenedor">
            ${this.productos.map(p => html `<div class="producto"><h3>${p.nombre}</h3><p>${p.descripcion}</p></div>`)}
      </div>
    `;
  }
  createRenderRoot(){
      return this;
  }

}

customElements.define('products-list', ProductsList);