const assert = require('assert');
const unavez = require('../unavez.js');
const sinon = require('sinon');

it("llama a la funcion original", function(){
    var callback = sinon.fake();
    var proxy = unavez.llamar(callback);

    proxy();
    proxy();

    assert(callback.calledOnce);
})