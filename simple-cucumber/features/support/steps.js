const {Given, When, Then} = require('@cucumber/cucumber');
const assert= require('assert');

let a,b,t;
let s;

Given('numbers {int} and {int}', function(i1, i2){
    a = i1;
    b = i2;
})

When ('they are added together', function(){
    t = a + b;
})

Then ('the total should be {int}', function(result){
    assert.strictEqual(t, result);
})