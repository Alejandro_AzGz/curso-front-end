import { LitElement, html, css } from 'lit-element';

class GetsetComponente  extends LitElement {


  static get properties() {
    return {
        prop : {type:Number}
    };
  }
  set prop(val){
      let oldVal = this.privateProp;
      this.privateProp= Math.floor(val);
  }
  get prop(val){
      return this.privateProp;
      this.privateProp =0;
  }

  constructor() {
    super();
    this.privateProp =0;
  }

  render() {
    return html`
      <p>Prop: ${this.prop}</p>
      <button @click="${() =>{this.prop=Math.random()*10; }}">Cambiar Prop </p>
    `;
  }
}

customElements.define('getset-componente', GetsetComponente);