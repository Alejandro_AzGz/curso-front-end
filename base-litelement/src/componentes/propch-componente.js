import { LitElement, html, css } from 'lit-element';

export class PropchComponente  extends LitElement {

  static get properties() {
    return {
        miprop: {type:Number,
                hasChanged(newVal, oldVal){
                    if (newVal > oldVal) {
                        console.log(`${newVal} > ${oldVal}. hasChanged: true`);
                        return true;
                    }else{
                        console.log(`${newVal} <= ${oldVal}. hasChanged: false`);
                        return false;
                    }
                }}
    };
  }

  constructor() {
    super();
    this.miprop= 1;
  }
  updated(){
      console.log("updated");
  }
  getNewVal(){
      let newVal = Math.floor(Math.random()*10);
      this.miprop= newVal;
  }

  render() {
    return html`
      <p>Prop</p>
    `;
  }
}

customElements.define('propch-componente', PropchComponente);