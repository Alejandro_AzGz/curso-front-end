import {LitElement, html} from 'lit-element';

class BoolComponent extends LitElement {
    static get properties(){
        return{
            message: {type:String},
            message2: {type:String},
            miBool: {type:Boolean}
        }
    }

    constructor(){
        super();
        this.message1 = "Mensaje privado";
        this.message2 = "Mensaje publico";
        this.miBool = false;
    }
    render(){
        return html`
            ${this.miBool?
             html`<div style="color:red">${this.message1}</div>`:
             html`<div>${this.message2}</div>`}
        `;
    }
}
customElements.define('bool-component', BoolComponent);