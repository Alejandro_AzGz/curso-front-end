import { LitElement, html, css } from 'lit-element';
import { PropchComponente} from './propch-componente.js';

class Event1Component  extends LitElement {

 

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <propch-componente @event-cambio="${(e) => {alert(e.detail.message);}}"></propch-componente>
    `;
  }
}

customElements.define('event1-component', Event1Component);