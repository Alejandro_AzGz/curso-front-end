import {LitElement, html} from 'lit-element';

class ArrComponent extends LitElement {
    static get properties(){
        return{
            navegadores: {type:Array}
        }
    }

    constructor(){
        super();
        this.navegadores = ["Chrome", "Safari", "Opera"];
    }
    render(){
        return html`
            <ul>
                ${this.navegadores.map(i => html`<li>${i}</li>`)}
            </ul>
        `;
    }
}
customElements.define('arr-component', ArrComponent);