import { LitElement, html, css } from 'lit-element';
import{buttonStyles} from './button-style.js'

class Button1Component  extends LitElement {

  static get styles() {
    return [
        buttonStyles,
        css`
      :host {
        display: block;
        border: 1px solid;
      }
    `];
  }

  static get properties() {
    return {};
  }

  constructor() {
    super();
  }

  render() {
    return html`
      <div><button class="blue-button">Click</button></div>
    `;
  }
}

customElements.define('button1-component', Button1Component);