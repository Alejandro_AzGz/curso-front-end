import { LitElement, html, css } from 'lit-element';

class Compuesto1Component  extends LitElement {

  headerTemplate(params) {
      return html`<p>${title};`
  }
  bodyTemplate(params) {
    return html`<p>${title};`
}

  static get properties() {
    return {
        title: {type:String},
        contenido: {type: String}
    };
  }

  constructor() {
    super();
    this.title ="titulo";
    this.contenido = "asdasdasd";
  }

  render() {
    return html`
      ${this.headerTemplate(this.title)}
      ${this.bodyTemplate(this.contenido)}
    `;
  }
}

customElements.define('compuesto1-component', Compuesto1Component);